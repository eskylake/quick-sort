<?php

ini_set('display_errors', 'On');

/**
 * This function print well formed information about a variable.
 * 
 * @param mixed $value the value to get its information.
 * 
 * @return void
 */
function pp($value = null)
{
    /**
     * Pre tag is used to define the block of preformatted text which preserves the text spaces,
     * line breaks, tabs, and other formatting characters which are ignored by web browsers.
     */
    echo "<pre>";
    print_r($value);
    echo "\n";
}


/**
 * This method gets an unsorted array of different values and sort it based on `quick sort` algorithm.
 * Base on `quick sort` algorithm rules:
 * 	- At first we must choose an index of the array as pivot.
 * 	  It can be any index of the array but it's better to choose the first index because of the calculation and speed.
 * 	- All values of the array that are less than the pivot must be put on the left side of it, and all the others that are greater than the pivot must be put on the right side.
 * 	- Then we have two separated arrays that the `sort()` method must be executed recursively on them until the remain arrays have only ONE value.
 *    Actually an array with only ONE value is already sorted.
 * 	- After all we must merge all arrays into each other to generate a sorted array.
 * 
 * @param array $array
 * 
 * @return array Sorted array
 */
function quickSort(array $array, string $sortType = 'asc')
{
    // If the array has only ONE value, then it's sorted and we must return it.
    if (count($array) <= 1) {
        return $array;
    }

    $pivot = $array[0];
    $leftSide = [];
    $rightSide = [];

    foreach ($array as $key => $value) {
        if ($value < $pivot) {
            $leftSide[] = $value;
        } elseif ($value > $pivot) {
            $rightSide[] = $value;
        }
    }
    
    if ($sortType == 'asc') {
        return [...quickSort($leftSide, $sortType), ...[$pivot], ...quickSort($rightSide, $sortType)];
    } else {
        return [...quickSort($rightSide, $sortType), ...[$pivot], ...quickSort($leftSide, $sortType)];
    }
}

// Usage
$array = [10, 2, 15, 50, 67, 20, 22, 3, 99, 9, 70, 101, 1, 30];
pp(quickSort($array, 'asc'));
pp(quickSort($array, 'desc'));