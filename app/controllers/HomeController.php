<?php

namespace App\controllers;

/**
 * Default controller class.
 * 
 * @author Ali Tavafi <ali.tavafii@gmail.com>
 */
class HomeController
{
    /**
     * Index action.
     * All routes without specifying the action are redirected to this action.
     * This action sorts an specific array based on `quick sort` algorithm in two different types and prints it out .
     *  - ascending
     *  - descending
     * 
     * @return void
     */
    public function index()
    {
        $array = [10, 2, 15, 50, 67, 20, 22, 3, 99, 9, 70, 101, 1, 30];
        
        $quickSort = new \App\models\QuickSort();

        pp(
            $quickSort->setSortType('asc')
                ->sort($array)
        );

        pp(
            $quickSort->setSortType('desc')
                ->sort($array)
        );
    }
}