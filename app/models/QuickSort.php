<?php

namespace App\models;

/**
 * @author Ali Tavafi <ali.tavafii@gmail.com>
 */
class QuickSort
{
	/**
	 * Set sort type. It can be `asc` or `desc`.
	 * 	- asc means ascending
	 * 	- desc means descending
	 * 
	 * @var string $sortType
	 */
    public string $sortType;

	/**
	 * The setter method for sort type property.
	 * Default sort type is `asc`.
	 * 
	 * @param string $type
	 * 
	 * @return \App\models\QuickSort
	 */
    public function setSortType(string $type = 'asc'): QuickSort
    {
        $this->sortType = $type;

        return $this;
    }

	/**
	 * This method gets an unsorted array of different values and sort it based on `quick sort` algorithm.
	 * Base on `quick sort` algorithm rules:
	 * 	- At first we must choose an index of the array as pivot.
	 * 	  It can be any index of the array but it's better to choose the first index because of the calculation and speed.
	 * 	- All values of the array that are less than the pivot must be put on the left side of it, and all the others that are greater than the pivot must be put on the right side.
	 * 	- Then we have two separated arrays that the `sort()` method must be executed recursively on them until the remain arrays have only ONE value.
	 *    Actually an array with only ONE value is already sorted.
	 * 	- After all we must merge all arrays into each other to generate a sorted array.
	 * 
	 * @param array $array
	 * 
	 * @return array Sorted array
	 */
    public function sort(array $array)
	{
		// If the array has only ONE value, then it's sorted and we must return it.
		if (count($array) <= 1) {
			return $array;
		}

        $pivot = $array[0];
        $leftSide = [];
        $rightSide = [];

		foreach ($array as $key => $value) {
			if ($value < $pivot) {
				$leftSide[] = $value;
			} elseif ($value > $pivot) {
				$rightSide[] = $value;
			}
        }
        
		if ($this->sortType == 'asc') {
			return [...$this->sort($leftSide), ...[$pivot], ...$this->sort($rightSide)];
		} else {
			return [...$this->sort($rightSide), ...[$pivot], ...$this->sort($leftSide)];
		}
    }
}